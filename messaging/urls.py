from django.conf.urls import patterns, include, url
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
  url(r'^$', 'messaging.views.home', name='home'),
  url(r'^about/?$', 'messaging.views.about', name='about'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
  url(r'^admin/', include(admin.site.urls)),
  url(r'^login$', 'messaging.apps.members.views.member_login', name='login'),
  url(r'^register$', 'messaging.apps.members.views.member_registration', name='register'),
  url(r'^logout$', 'messaging.apps.members.views.member_logout', name='logout'),
  url(r'^user/', include('messaging.apps.members.urls')),
  url(r'^inbox/', include('messaging.apps.messages.urls')),
  url(r'^groups/', include('messaging.apps.groups.urls')),
)

if settings.DEBUG:
  urlpatterns += patterns('',
    (r'^media/(?P<path>.*)$','django.views.static.serve', {'document_root':settings.MEDIA_ROOT})
    )






