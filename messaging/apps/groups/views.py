# Create your views here.
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.template import RequestContext
from messaging.apps.members.models import Member
from django.core.urlresolvers import reverse
from models import Group
import json

def group_list(request):
  member = request.user.get_profile()
  if not request.user.is_authenticated():
    return HttpResponseRedirect(reverse('login'))
  groups = Group.objects.all()
  context = {}
  context['groups'] = groups
  context['member'] = member
  return render_to_response('groups/list.html',context,context_instance=RequestContext(request))


def group_join(request, group_pk):
  member = request.user.get_profile()
  group = get_object_or_404(Group, pk=group_pk)
  if group not in member.groups.all():
    member.groups.add(group)
  return HttpResponseRedirect(reverse('home'))


