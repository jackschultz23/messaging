from django.conf.urls import patterns, url

urlpatterns = patterns('messaging.apps.groups.views',
  url(r'^$', 'group_list', name='group_list'),
  url(r'^(?P<group_pk>\d+)/join$', 'group_join', name='group_join'),
)
