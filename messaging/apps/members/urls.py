from django.conf.urls import patterns, url

urlpatterns = patterns('messaging.apps.members.views',
  url(r'^$', 'member_profile', name='member_profile'),
)
