# Create your views here.
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.template import RequestContext
from forms import RegistrationForm, LoginForm
from models import Member
from django.core.urlresolvers import reverse
from messaging.apps.groups.models import Group

def member_registration(request):
  if request.user.is_authenticated():
    return HttpResponseRedirect(reverse('member_profile'))
  if request.method == 'POST':
    form = RegistrationForm(request.POST)
    if form.is_valid():
      user = User.objects.create_user(username=form.cleaned_data['username'],email=None,password=form.cleaned_data['password'])
      user.save()
      member = user.get_profile()
      #here we would put all the custom fields if we had them. nothing now
      all_group = Group.objects.filter(name="all_members")[0] #probably getter way to get this group, like id=1 or somethign
      member.groups.add(all_group)
      member.save()
      new_member = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
      login(request, new_member)
      return HttpResponseRedirect(reverse('home'))
    else:
      return render_to_response('members/register.html',{'form':form},context_instance=RequestContext(request))
  else:
    form = RegistrationForm
    context = {'form':form}
    return render_to_response('members/register.html',context,context_instance=RequestContext(request))

def member_login(request):
  if request.user.is_authenticated():
    return HttpResponseRedirect(reverse('member_profile'))
  if request.method == 'POST':
    form = LoginForm(request.POST)
    username = request.POST['username']
    password = request.POST['password']
    member = authenticate(username=username, password=password)
    if form.is_valid():
      username = form.cleaned_data['username']
      password = form.cleaned_data['password']
      member = authenticate(username=username, password=password)
      if member:
        login(request, member)
        return HttpResponseRedirect(reverse('home'))
      else:
        return render_to_response('members/login.html',{'form':form},context_instance=RequestContext(request))
    else:
      return render_to_response('members/login.html',{'form':form},context_instance=RequestContext(request))
  else:
    form = LoginForm()
    context = {'form': form}
    return render_to_response('members/login.html',context,context_instance=RequestContext(request))

def member_logout(request):
  logout(request)
  return HttpResponseRedirect(reverse('home'))

def member_profile(request):
  return HttpResponseRedirect(reverse('home'))
