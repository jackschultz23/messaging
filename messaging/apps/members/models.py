from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from messaging.apps.groups.models import Group
from django.db import IntegrityError
import uuid
# Create your models here.

class Member(models.Model):
  user = models.OneToOneField(User)
  groups = models.ManyToManyField(Group, related_name='members')

  def save(self, *args, **kwargs):
    if not self.pk:
      while True:
        self.pk = int(str(uuid.uuid4().int)[0:6])
        try:
          super(Member,self).save(*args, **kwargs)
          break
        except IntegrityError as e:
          print e
          continue
    else:
      super(Member,self).save(*args, **kwargs)

  def __unicode__(self):
    return self.user.username

def create_callback(sender, instance, **kwargs):
  user, new = Member.objects.get_or_create(user=instance)

post_save.connect(create_callback, User)


