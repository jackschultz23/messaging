# Create your views here.
from django.http import HttpResponseRedirect, HttpResponse, Http404, HttpResponseForbidden
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.template import RequestContext
from forms import MessageCreateForm
from messaging.apps.members.models import Member
from messaging.apps.groups.models import Group
from django.core.urlresolvers import reverse
from models import Message, MessageInfo
import json
from tasks import create_message_info


def message_inbox(request):
  if not request.user.is_authenticated():
    return HttpResponseRedirect(reverse('login'))
  member = request.user.get_profile()
  messages = member.message_info.all()
  context = {}
  context['messages'] = messages
  return render_to_response('messages/inbox.html',context,context_instance=RequestContext(request))


def message_create(request):
  try:
    member = request.user.get_profile()
  except AttributeError:
    return HttpResponseRedirect('/login')
  if request.method == 'POST':
    form = MessageCreateForm(request.POST)
    if form.is_valid():
      #check if group or user
      try:
        g_or_u = form.cleaned_data['to'].split(' ')
      except:
        raise Http404
      user_id = []
      member_send = None
      group_send = None
      if g_or_u[0] == "User:":
        member_send = g_or_u[1]
      elif g_or_u[0] == "Group:":
        group_send = g_or_u[1]
      else: #need to try to guess the user since autocomplete
        member_send = g_or_u[0]
        group_send = g_or_u[0]
      #now we want to try to get the member or group object
      member_send = Member.objects.filter(user__username = member_send)
      group_send = Group.objects.filter(name = group_send)
      #there is a possibility of having both a member and group be the same
      message = None
      if member_send:
        member_to = member_send[0]
        user_id.append(member_to.pk)
        message = Message(subject=form.cleaned_data['subject'],sender=request.user.get_profile(),body=form.cleaned_data['body'],recipient_member=member_to)
        message.save()
      elif group_send:
        group_to = group_send[0]
        if group_to not in member.groups.all(): #reject if not member of that group
          return HttpResponseRedirect(reverse('message_inbox'))
        for user in group_to.members.all():
          if user.pk != member.pk:
            user_id.append(user.pk)
        message = Message(subject=form.cleaned_data['subject'],sender=request.user.get_profile(),body=form.cleaned_data['body'],recipient_group=group_to)
        message.save()
      if message:
        #now we need to work on the message infos.
        message_info = MessageInfo(message=message, member=member,read=True)
        message_info.save()
        create_message_info.delay(message.id, user_id)
        #create the message, and send off to the queue to actually create the messages
        return HttpResponseRedirect(reverse('message_inbox'))
      else:
        context = {'form':form}
        context['error'] = 'that person/group does not exist, or you do not have access to message them. Sorry'

        return render_to_response('messages/create.html',context,context_instance=RequestContext(request))
  else:
    form = MessageCreateForm
    context = {'form':form}
    return render_to_response('messages/create.html',context,context_instance=RequestContext(request))


def message_overview(request, message_pk):
  if not request.user.is_authenticated():
    return HttpResponseRedirect(reverse('login'))
  member = request.user.get_profile()
  message = member.message_info.get(pk=message_pk)
  if member != message.member:
    raise Http404
  message.read = True
  message.save()
  context = {}
  context['message'] = message
  return render_to_response('messages/overview.html',context,context_instance=RequestContext(request))

def message_autocomplete(request):
  if not request.user.is_authenticated():
    return HttpResponseForbidden
  member = request.user.get_profile()
  q = request.GET.get('q',None)
  if q is None:
    raise Http404
  members = Member.objects.filter(user__username__startswith=q)
  usernames = [member.user.username for member in members]
  response_data = {}
  for username in usernames:
    response_data[username] = "User: " + username
  #only send to groups where he's a member
  groups = member.groups.filter(name__startswith=q)
  groupnames = [group.name for group in groups]
  for group in groupnames:
    response_data[group] = "Group: " + group
  return HttpResponse(json.dumps(response_data), content_type="application/json")
  

def message_delete(request, message_pk):
  if not request.user.is_authenticated():
    return HttpResponseRedirect(reverse('login'))
  member = request.user.get_profile()
  message = member.message_info.get(pk=message_pk)
  if member != message.member:
    raise Http404
  message.delete()
  return HttpResponseRedirect(reverse('message_inbox'))
