from celery import task
from models import Message, MessageInfo
from messaging.apps.members.models import Member

@task() 
def create_message_info(message_id, to_list):
  message = Message.objects.get(id=message_id)
  for uid in to_list:
    user = Member.objects.get(pk=uid)
    message_info = MessageInfo(message=message, member=user)
    message_info.save()


