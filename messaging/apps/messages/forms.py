from django import forms
from django.forms import ModelForm
from models import Message
from django.contrib.auth import authenticate, login, logout

class MessageCreateForm(ModelForm):
  to = forms.CharField(label=(u'To'))
  body = forms.CharField(label=(u'Body'), widget=forms.Textarea(attrs={'cols': 80, 'rows': 10}))
  subject = forms.CharField(label=(u'Subject'))

  class Meta:
    model = Message
    exclude = ('sender','recipient_member','recipient_group','time_sent')

