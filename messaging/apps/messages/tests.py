"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from django.test import TestCase
from messaging.apps.members.models import Member
from messaging.apps.groups.models import Group
from django.contrib.auth.models import User
from models import Message
from django.test.client import Client



class SimpleTest(TestCase):

    def setUp(self):
      asdf = User(username='asdf',email="asdf@asdf.com",password='asdf')
      asdf.save()
      asdf2 = User.objects.create(username='asdf2',email="asdf2@asdf.com",password='asdf')
      asdf3 = User.objects.create(username='asdf3',email="asdf3@asdf.com",password='asdf')
      group = Group.objects.create(name='all_members')
      message = Message.objects.create(subject="really?",sender=asdf.get_profile(),body="words have chars in them",recipient_member=asdf2.get_profile())

    def test_message_send(self):
      asdf2 = Member.objects.filter(user__username='asdf2')[0]
      asdf = Member.objects.filter(user__username='asdf')[0]
      mess = Message.objects.all()[0].recipient_member.message_received.all()[0]
      sender1 = Message.objects.all()[0].sender
      asdf = User.objects.get(username='asdf').get_profile()

      mess1 = Message.objects.all()[0]
      messages = asdf.message_sent.all()
      print messages

      #testing the sender is correct
      self.assertEqual(asdf,sender1)
      #that the message is in the sent list
      self.assertTrue(mess in messages)


    def test_user_register(self):
      asdf2 = Member.objects.filter(user__username='asdf2')[0]
      resp = self.client.post('/register',{'username':'asdf5','email':'asdf5@asdf5.com','password':'asdf5','password1':'asdf5'})
      self.assertEqual(resp.status_code,302) #redirect after logged in

      self.assertEqual(len(Message.objects.all()),1) #the one in init
      resp = self.client.post('/inbox/create',{'to':'asdf','body':'this is a body','subject':'this is a subject'})
      self.assertEqual(len(Message.objects.all()),2) #now the new one
      
      resp = self.client.post('/inbox/create',{'to':'all_members','body':'this is a body','subject':'to everyone!!'})
      mess_to_all = Message.objects.filter(subject='to everyone!!')[0]
      self.assertTrue(mess_to_all in Message.objects.all()) #now the new one
      
      mem = Member.objects.filter(user__username='asdf5')[0] #check that the message is in everyone's imbox
      self.assertEqual(len(mem.message_info.all()),2) #now the new one

