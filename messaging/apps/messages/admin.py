from django.contrib import admin
from models import Message, MessageInfo

admin.site.register(Message)
admin.site.register(MessageInfo)
