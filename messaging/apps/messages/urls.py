from django.conf.urls import patterns, url

urlpatterns = patterns('messaging.apps.messages.views',
  url(r'^$', 'message_inbox', name='message_inbox'),
  url(r'^autocomplete$', 'message_autocomplete', name='message_autocomplete'),
  url(r'^create$', 'message_create', name='message_create'),
  url(r'^(?P<message_pk>\d+)$', 'message_overview', name='message_overview'),
  url(r'^(?P<message_pk>\d+)/delete$', 'message_delete', name='message_delete'),
)
