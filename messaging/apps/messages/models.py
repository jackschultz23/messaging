from django.db import models
from messaging.apps.members.models import Member
from messaging.apps.groups.models import Group
from django.db import IntegrityError
import datetime
import uuid

class Message(models.Model):
  subject = models.CharField(max_length=40)
  sender = models.ForeignKey(Member, related_name='message_sent')
  body = models.TextField()
  recipient_member = models.ForeignKey(Member, null=True, blank=True, related_name='message_received')
  recipient_group = models.ForeignKey(Group, null=True, blank=True, related_name='message')
  time_sent = models.DateTimeField(default=datetime.datetime.now)

  def save(self, *args, **kwargs):
    len_id = 6
    if not self.pk:
      while True:
        self.pk = int(str(uuid.uuid4().int)[0:len_id])
        try:
          super(Message,self).save(*args, **kwargs)
          break
        except IntegrityError as e:
          if len_id < len(str(uuid.uuid4())):
            len_id += 1
          continue
    else:
      super(Message,self).save(*args, **kwargs)

  def __unicode__(self):
    return self.subject

class MessageInfo(models.Model):
  read = models.BooleanField(default=False)
  member = models.ForeignKey(Member, related_name='message_info')
  message = models.ForeignKey(Message, related_name='message_info')

  def save(self, *args, **kwargs):
    len_id = 6
    if not self.pk:
      while True:
        self.pk = int(str(uuid.uuid4().int)[0:len_id])
        try:
          super(MessageInfo,self).save(*args, **kwargs)
          break
        except IntegrityError as e:
          if len_id < len(str(uuid.uuid4())):
            len_id += 1
          continue
    else:
      super(MessageInfo,self).save(*args, **kwargs)


