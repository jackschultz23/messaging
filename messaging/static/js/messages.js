$(document).ready(function() {


  
  $("#id_to").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url:"/inbox/autocomplete",
        minLength: 2,
        dataType: "json",
        data: {//query params
          q:request.term,
        },
        success: function( data ) {
          response( $.map( data, function(item) {
            return {
              label: item,
              value: item,
            }
          }));
        }
      });
    },
    minLength: 3,
    open: function() {
      $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
      $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

});






